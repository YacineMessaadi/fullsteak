/* eslint-disable @next/next/no-img-element */
import { Notyf } from "notyf";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"; // Import the FontAwesomeIcon component
import { faCheckCircle, faLock } from "@fortawesome/free-solid-svg-icons"; // import the icons you need
import axios from "axios";
import React from "react";
import { useRouter } from "next/router";

export default function ResetPassword() {
  const router = useRouter();
  async function handleReset(event) {
    const notyf = new Notyf({
      duration: 5000,
      position: {
        x: "center",
        y: "bottom",
      },
    });
    event.preventDefault();
    const urlParams = new URLSearchParams(window.location.search);
    const code = urlParams.get("code");
    const reset = await axios
      .post(process.env.SERVER + "/api/auth/reset", {
        code: code,
        password: event.target.password.value,
        passwordConfirmation: event.target.password.value,
      })
      .then((res) => {
        notyf.success("Mot de passe changé ! Redirection...");
        router.push("/login");
      })
      .catch((_) => {
        notyf.error("Une erreur est survenue ! Réessayez");
      });
  }

  return (
    <div className="min-h-screen flex items-center justify-center bg-gray-50 py-12 px-4 sm:px-6 lg:px-8">
      <div className="max-w-md w-full space-y-8">
        <div>
          <img className="mx-auto h-12 w-auto" src="/w.png" alt="Wrappr logo" />
          <h2 className="mt-6 text-center text-3xl font-extrabold text-gray-900">
            Entrez votre nouveau mot de passe
          </h2>
        </div>
        <form
          className="mt-8 space-y-6"
          action="#"
          method="POST"
          onSubmit={handleReset}
        >
          <input type="hidden" name="remember" defaultValue="true" />
          <div className="rounded-md shadow-sm -space-y-px">
            <div>
              <label htmlFor="password" className="sr-only">
                Nouveau mot de passe
              </label>
              <input
                id="password"
                name="password"
                type="password"
                autoComplete="current-password"
                required
                className="appearance-none rounded-none relative block w-full px-3 py-2 border border-gray-300 placeholder-gray-500 text-gray-900 rounded-b-md focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 focus:z-10 sm:text-sm"
                placeholder="Nouveau mot de passe"
              />
            </div>
          </div>
          <div>
            <button
              type="submit"
              className="group relative w-full flex justify-center py-2 px-4 border border-transparent text-sm font-medium rounded-md text-white bg-wblue hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
            >
              <span className="absolute left-0 inset-y-0 flex items-center pl-3">
                <FontAwesomeIcon icon={faCheckCircle}></FontAwesomeIcon>
              </span>
              Confirmer
            </button>
          </div>
        </form>
      </div>
    </div>
  );
}
