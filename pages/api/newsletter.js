import axios from "axios";

export default function handler(req, res) {
  axios
    .post("https://api.fullsteak.fr/leads", { email: req.body.email })
    .then((e) => res.status(200).json({ message: e.status }));
}
