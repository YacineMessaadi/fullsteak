import axios from "axios";

export default function handler(req, res) {
  axios
    .post("https://api.fullsteak.fr/auth/local/register", {
      username: req.body.username,
      email: req.body.email,
      password: req.body.password,
    })
    .then((response) => {
      res.status(201).json({ message: "Registered" });
    })
    .catch((error) => {
      res.status(501).json({ message: "KO" });
    });
}
