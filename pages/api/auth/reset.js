import axios from "axios";

export default function handler(req, res) {
  axios
    .post("https://api.fullsteak.fr/auth/reset-password", {
      code: req.body.code,
      password: req.body.password,
      passwordConfirmation: req.body.password,
    })
    .then((response) => {
      res.status(200).json({ message: "mdp changé" });
    })
    .catch((error) => {
      res.status(500).json({ message: error.response });
    });
}
