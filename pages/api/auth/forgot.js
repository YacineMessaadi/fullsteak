import axios from "axios";

export default function handler(req, res) {
  axios
    .post("https://api.fullsteak.fr/auth/forgot-password", {
      email: req.body.email,
    })
    .then((_) => {
      res.status(200).json({ message: "email envoyé" });
    })
    .catch((error) => {
      res.status(error.response.status).json({ message: error.message });
    });
}
