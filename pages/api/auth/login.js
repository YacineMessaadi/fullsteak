import axios from "axios";

export default function handler(req, res) {
  axios
    .post("https://api.fullsteak.fr/auth/local", {
      identifier: req.body.identifier,
      password: req.body.password,
    })
    .then((response) => {
      if (response.status == 200) {
        res.status(200).json(response.data);
      } else throw Error("ko");
    })
    .catch((error) => {
      res.status(500).json({ message: "ko" });
    });
}
