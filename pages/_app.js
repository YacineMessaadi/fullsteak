import "../styles/globals.css";
import "../styles/article.css";
import "@fortawesome/fontawesome-svg-core/styles.css"; // import Font Awesome CSS
import "notyf/notyf.min.css";
import { config } from "@fortawesome/fontawesome-svg-core";
config.autoAddCss = false; // Tell Font Awesome to skip adding the CSS automatically since it's being imported above
import React from "react";
import Head from "next/head";
import App from "next/app";
import AppContext from "../context/AppContext";
import axios from "axios";
import Cookies from "js-cookie";
import Router from "next/router";
import * as ga from "../utils/ga";

class MyApp extends App {
  state = {
    user: null,
  };

  componentDidMount() {
    // grab token value from cookie
    const token = Cookies.get("jwt");

    if (token) {
      // authenticate the token on the server and place set user object
      axios
        .get(`https://www.api.fullsteak.fr/users/me`, {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        })
        .then((res) => {
          // if res comes back not valid, token is not valid
          // delete the token and log the user out on client
          if (res.status != 200) {
            Cookies.remove("user");
            Cookies.remove("jwt");
            this.setState({ user: null });
            return null;
          }
          const user = res.data;
          this.setUser(user);
        })
        .catch((e) => {
          console.log(e);
        });
    }

    const handleRouteChange = (url) => {
      ga.pageview(url);
    };
    //When the component is mounted, subscribe to router changes
    //and log those page views
    Router.events.on("routeChangeComplete", handleRouteChange);

    // If the component is unmounted, unsubscribe
    // from the event with the `off` method
    return () => {
      Router.events.off("routeChangeComplete", handleRouteChange);
    };
  }

  setUser = (user) => {
    this.setState({ user });
  };

  render() {
    const { Component, pageProps } = this.props;
    return (
      <AppContext.Provider
        value={{
          user: this.state.user,
          isAuthenticated: !!this.state.user,
          setUser: this.setUser,
        }}
      >
        <Head>
          <title>FullSteak</title>
          <meta name="description" content="Le guide du dev fullstack" />
          <link rel="icon" href="/favicon.ico" />
          {/* Global Site Tag (gtag.js) - Google Analytics */}
          <script
            async
            src={`https://www.googletagmanager.com/gtag/js?id=${process.env.NEXT_PUBLIC_GOOGLE_ANALYTICS}`}
          />
          <script
            dangerouslySetInnerHTML={{
              __html: `
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());
            gtag('config', '${process.env.NEXT_PUBLIC_GOOGLE_ANALYTICS}', {
              page_path: window.location.pathname,
            });
          `,
            }}
          />
        </Head>

        <Component {...pageProps} />
      </AppContext.Provider>
    );
  }
}

export default MyApp;
