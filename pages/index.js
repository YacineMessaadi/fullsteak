import axios from "axios";
import Head from "next/head";
import Latest from "../components/latest-blog";
import Navbar from "../components/navbar";

export default function Home(props) {
  return (
    <div className="">
      <Navbar />

      <main className="mt-6">
        <Latest articles={props.articles} />
      </main>

      <footer className="footer"></footer>
    </div>
  );
}

export async function getServerSideProps(context) {
  const articles = await axios
    .get("https://api.fullsteak.fr/articles")
    .then((res) => {
      return res.data;
    });

  return {
    props: {
      articles,
    },
  };
}
