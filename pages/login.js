/* eslint-disable @next/next/no-img-element */
import Link from "next/link";
import { useRouter } from "next/router";
import Cookies from "js-cookie";
import { Notyf } from "notyf";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"; // Import the FontAwesomeIcon component
import { faLock } from "@fortawesome/free-solid-svg-icons"; // import the icons you need
import axios from "axios";
import React, { useState, useContext } from "react";
import AppContext from "../context/AppContext";

function validateEmail(email) {
  var re =
    /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(email);
}

export default function Login() {
  const context = useContext(AppContext);
  const [email, setEmail] = useState("");
  const router = useRouter();
  context.user ? router.push("/") : "";

  async function handleLogin(event) {
    const notyf = new Notyf({
      duration: 3500,
      position: {
        x: "center",
        y: "bottom",
      },
    });
    event.preventDefault();
    const login = await axios
      .post("/api/auth/login", {
        identifier: event.target.email.value,
        password: event.target.password.value,
      })
      .then((res) => {
        if (res.status == 200) {
          Cookies.set("jwt", res.data.jwt, { expires: 1 });
          Cookies.set("user", JSON.stringify(res.data.user), { expires: 1 });
          notyf.success(`Bienvenue ${res.data.user.username} !`);

          setTimeout(() => {
            window.location.href = "/";
          }, 2000);
        } else {
          throw Error("ko");
        }
      })
      .catch((error) => {
        notyf.error("Une erreur est survenue ! Réessayez");
      });
  }

  return (
    <div className="min-h-screen flex items-center justify-center bg-gray-50 py-12 px-4 sm:px-6 lg:px-8">
      <div className="max-w-md w-full space-y-8">
        <div>
          <img className="mx-auto w-32" src="/steak.svg" alt="Wrappr logo" />
          <h2 className="mt-6 text-center text-3xl font-extrabold text-gray-900">
            Connectez-vous
          </h2>
          <p className="mt-2 text-center text-sm text-gray-600">
            Ou{" "}
            <Link href="/register">
              <a className="font-medium text-wblue hover:text-indigo-500">
                inscrivez-vous
              </a>
            </Link>
          </p>
        </div>
        <form
          className="mt-8 space-y-6"
          action="#"
          method="POST"
          onSubmit={handleLogin}
        >
          <input type="hidden" name="remember" defaultValue="true" />
          <div className="rounded-md shadow-sm -space-y-px">
            <div>
              <label htmlFor="email" className="sr-only">
                Email
              </label>
              <input
                onChange={(e) => {
                  setEmail(e.target.value);
                }}
                id="email"
                name="email"
                type="email"
                autoComplete="email"
                required
                className="appearance-none rounded-none relative block w-full px-3 py-2 border border-gray-300 placeholder-gray-500 text-gray-900 rounded-t-md focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 focus:z-10 sm:text-sm"
                placeholder="Adresse email"
              />
            </div>
            <div>
              <label htmlFor="password" className="sr-only">
                Mot de passe
              </label>
              <input
                id="password"
                name="password"
                type="password"
                autoComplete="current-password"
                required
                className="appearance-none rounded-none relative block w-full px-3 py-2 border border-gray-300 placeholder-gray-500 text-gray-900 rounded-b-md focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 focus:z-10 sm:text-sm"
                placeholder="Mot de passe"
              />
            </div>
          </div>

          <div className="flex items-center justify-center">
            <div className="text-sm">
              <a
                onClick={async () => {
                  const notyf = new Notyf({
                    duration: 3500,
                    position: {
                      x: "center",
                      y: "bottom",
                    },
                  });
                  if (!validateEmail(email)) {
                    notyf.error("Adresse email non valide");
                  } else {
                    await axios
                      .post(process.env.SERVER + "/api/auth/forgot", {
                        email: email,
                      })
                      .then((res) => {
                        if (res.status == 200) {
                          notyf.success(
                            "Un mail de récupération vient de vous être envoyé"
                          );
                        } else {
                          throw Error("ko");
                        }
                      })
                      .catch((error) => {
                        if (error.response.status == 400)
                          notyf.error("Adresse email inconnue");
                        else notyf.error("Une erreur est survenue !");
                      });
                  }
                }}
                className="font-medium text-wblue hover:text-indigo-500 cursor-pointer"
              >
                J&apos;ai oublié mon mot de passe
              </a>
            </div>
          </div>

          <div>
            <button
              type="submit"
              className="group relative w-full flex justify-center py-2 px-4 border border-transparent text-sm font-medium rounded-md text-blood bg-gold hover:bg-blood hover:text-gold focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-red-600"
            >
              <span className="absolute left-0 inset-y-0 flex items-center pl-3">
                <FontAwesomeIcon icon={faLock}></FontAwesomeIcon>
              </span>
              Se connecter
            </button>
          </div>
        </form>
      </div>
    </div>
  );
}
