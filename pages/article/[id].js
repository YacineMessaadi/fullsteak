/* eslint-disable @next/next/no-img-element */
import Head from "next/head";
import axios from "axios";
import ReactMarkdown from "react-markdown";
import Navbar from "../../components/navbar";

export default function Article(props) {
  const content = props.article.content;

  return (
    <div>
      <Navbar />
      <main className="relative container mx-auto bg-white px-4">
        <div className="relative -mx-4 top-0 pt-[17%] overflow-hidden rounded-t-none rounded-2xl">
          <img
            className="absolute inset-0 object-cover object-top w-full h-full filter blur"
            src={`https://images.unsplash.com/photo-1592503254549-d83d24a4dfab?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=2064&q=80`}
            alt=""
          />
        </div>

        <div className="mt-[-10%] w-1/2 mx-auto">
          <div className="relative pt-[56.25%] overflow-hidden rounded-2xl">
            <img
              className="w-full h-full absolute inset-0 object-cover"
              src={`https://api.fullsteak.fr${props.article.image.formats.medium.url}`}
              alt=""
            />
          </div>
        </div>

        <article className="prose max-w-prose mx-auto py-8">
          <ReactMarkdown>{content}</ReactMarkdown>
        </article>
      </main>
    </div>
  );
}

export async function getServerSideProps(context) {
  const { id } = context.query;

  const article = await axios
    .get("https://api.fullsteak.fr/articles/" + id)
    .then((res) => {
      return res.data;
    });

  return {
    props: {
      article,
    },
  };
}
