/* eslint-disable @next/next/no-img-element */
import Link from "next/link";
import { useContext } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"; // Import the FontAwesomeIcon component
import { faArrowCircleUp, faUpload } from "@fortawesome/free-solid-svg-icons"; // import the icons you need
import axios from "axios";
import { Notyf } from "notyf";
import AppContext from "../context/AppContext";
import { useRouter } from "next/router";

export default function Register() {
  const context = useContext(AppContext);
  const router = useRouter();
  context.user ? router.push("/") : "";
  return (
    <div className="min-h-screen flex items-center justify-center bg-gray-50 py-12 px-4 sm:px-6 lg:px-8">
      <div className="max-w-md w-full space-y-8">
        <div>
          <img className="mx-auto w-32" src="/steak.svg" alt="Wrappr logo" />
          <h2 className="mt-6 text-center text-3xl font-extrabold text-gray-900">
            Inscrivez-vous
          </h2>
          <p className="mt-2 text-center text-sm text-gray-600">
            Ou{" "}
            <Link href="/login">
              <a className="font-medium text-wblue hover:text-indigo-500">
                connectez-vous
              </a>
            </Link>
          </p>
        </div>
        <form
          className="mt-8 space-y-6"
          action="#"
          method="POST"
          onSubmit={handleRegister}
        >
          <input type="hidden" name="remember" defaultValue="true" />
          <div className="rounded-md shadow-sm -space-y-px">
            <div>
              <label htmlFor="email" className="sr-only">
                Nom
              </label>
              <input
                id="name"
                name="name"
                type="text"
                autoComplete="name"
                required
                className="appearance-none rounded-none relative block w-full px-3 py-2 border border-gray-300 placeholder-gray-500 text-gray-900 rounded-t-md focus:outline-none focus:ring-wblue focus:border-wblue focus:z-10 sm:text-sm"
                placeholder="Prénom Nom"
              />
            </div>
            <div>
              <label htmlFor="email" className="sr-only">
                Email
              </label>
              <input
                id="email"
                name="email"
                type="email"
                autoComplete="email"
                required
                className="appearance-none rounded-none relative block w-full px-3 py-2 border border-gray-300 placeholder-gray-500 text-gray-900 rounded-t-md focus:outline-none focus:ring-wblue focus:border-wblue focus:z-10 sm:text-sm"
                placeholder="Adresse email"
              />
            </div>
            <div>
              <label htmlFor="password" className="sr-only">
                Mot de passe
              </label>
              <input
                id="password"
                name="password"
                type="password"
                autoComplete="current-password"
                required
                className="appearance-none rounded-none relative block w-full px-3 py-2 border border-gray-300 placeholder-gray-500 text-gray-900 rounded-b-md focus:outline-none focus:ring-wblue focus:border-wblue focus:z-10 sm:text-sm"
                placeholder="Mot de passe"
              />
            </div>
          </div>

          <div>
            <button
              type="submit"
              className="group relative w-full flex justify-center py-2 px-4 border border-transparent text-sm font-medium rounded-md text-blood bg-gold hover:bg-blood hover:text-gold focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-red-600"
            >
              <span className="absolute right-0 inset-y-0 flex items-center pr-3">
                <FontAwesomeIcon icon={faArrowCircleUp}></FontAwesomeIcon>
              </span>
              S&apos;inscrire
            </button>
          </div>
        </form>
      </div>
    </div>
  );
}

async function handleRegister(event) {
  event.preventDefault();
  const notyf = new Notyf({
    duration: 5000,
    position: {
      x: "center",
      y: "bottom",
    },
  });
  const register = await axios
    .post("/api/auth/register", {
      username: event.target.name.value,
      email: event.target.email.value,
      password: event.target.password.value,
    })
    .then((res) => {
      if (res.status == 201) {
        notyf.success("Merci ! Un email de confirmation vous a été envoyé !");
      }
    })
    .catch((_) => {
      notyf.error("Une erreur est survenue ! Réessayez");
    });
}
