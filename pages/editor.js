/* eslint-disable @next/next/no-img-element */
/* eslint-disable jsx-a11y/alt-text */
import "@uiw/react-md-editor/markdown-editor.css";
import "@uiw/react-markdown-preview/markdown.css";
import dynamic from "next/dynamic";
import { useState, useContext, useEffect } from "react";
import { useRouter } from "next/router";
import Navbar from "../components/navbar";
import AppContext from "../context/AppContext";
import axios from "axios";
import { Notyf } from "notyf";
import Cookies from "js-cookie";

const MDEditor = dynamic(
  () => import("@uiw/react-md-editor").then((mod) => mod.default),
  { ssr: false }
);

function Editor() {
  const context = useContext(AppContext);
  const router = useRouter();

  useEffect(() => {
    if (!context.isAuthenticated) {
      router.push("/login");
    }
  });

  const [image, setImage] = useState(null);
  const [value, setValue] = useState(
    '```javascript\nconst verite = "Le JS est un langage fantastique";\nalert(verite);\n```'
  );

  function handleChange(event) {
    setImage(event.target.files[0]);
  }

  function handleSubmit(event) {
    event.preventDefault();
    const notyf = new Notyf();

    const formData = new FormData();

    formData.append("files", image);

    axios
      .post("https://api.fullsteak.fr/upload", formData, {
        headers: {
          Authorization: "Bearer " + Cookies.get("jwt"),
        },
      })
      .then((response) => {
        const imageId = response.data[0].id;
        const request = {
          title: event.target.title.value,
          author: context.user.id,
          content: value,
          description: event.target.description.value,
          steak: event.target.steak.value.split("; "),
          image: imageId,
        };
        axios
          .post("https://api.fullsteak.fr/articles", request, {
            headers: {
              Authorization: "Bearer " + Cookies.get("jwt"),
            },
          })
          .then((res) => {
            notyf.success("Article publié !");
          })
          .catch((error) => {
            notyf.error("Vérifiez les champs !");
          });
      })
      .catch((error) => {
        notyf.error("Vérifiez les champs !");
      });
  }

  return (
    <div>
      <Navbar />
      <div className="flex items-center justify-center">
        <form
          className="bg-white shadow-md rounded mt-5 px-8 pt-6 pb-8 mb-5 w-3/4"
          onSubmit={handleSubmit}
        >
          <div className="mb-5">
            <label
              className="block text-gray-700 text-sm font-bold mb-2"
              htmlFor="title"
            >
              Titre
            </label>
            <input
              required
              className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
              id="title"
              name="title"
              type="text"
              placeholder="Ton article qui tue"
            />
          </div>
          <div className="mb-5">
            <label
              className="block text-gray-700 text-sm font-bold mb-2"
              htmlFor="steak"
            >
              Steak technique
            </label>
            <input
              required
              className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
              id="steak"
              type="text"
              name="steak"
              placeholder="Next.JS; Flutter; Tailwind CSS"
            />
          </div>
          <div className="mb-5">
            <label
              className="block text-gray-700 text-sm font-bold mb-2"
              htmlFor="description"
            >
              Description
            </label>
            <input
              required
              className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
              id="description"
              name="description"
              type="text"
              placeholder="Écris une petite description..."
            />
          </div>
          <label
            className="block text-gray-700 text-sm font-bold mb-2"
            htmlFor="content"
          >
            Contenu
          </label>
          <MDEditor id="content" value={value} onChange={setValue} />
          <div id="file-upload" className="flex justify-center mt-8">
            <div className="max-w-2xl rounded-lg shadow-xl bg-gray-50">
              <div className="m-4">
                <label className="inline-block mb-2 text-gray-700 text-sm font-bold ">
                  Illustration
                </label>
                <div className="flex items-center justify-center w-full">
                  <label className="flex flex-col w-full h-32 border-4 border-blue-200 border-dashed hover:bg-gray-100 hover:border-gray-300">
                    {image !== null ? (
                      <img
                        className="object-contain w-full h-32 p-4"
                        src={URL.createObjectURL(image)}
                      />
                    ) : (
                      <div className="flex flex-col items-center justify-center pt-7">
                        <svg
                          xmlns="http://www.w3.org/2000/svg"
                          className="w-8 h-8 text-gray-400 group-hover:text-gray-600"
                          fill="none"
                          viewBox="0 0 24 24"
                          stroke="currentColor"
                        >
                          <path
                            strokeLinecap="round"
                            strokeLinejoin="round"
                            strokeWidth="2"
                            d="M7 16a4 4 0 01-.88-7.903A5 5 0 1115.9 6L16 6a5 5 0 011 9.9M15 13l-3-3m0 0l-3 3m3-3v12"
                          />
                        </svg>
                        <p className="pt-1 text-sm tracking-wider text-gray-400 group-hover:text-gray-600">
                          Ajouter une image
                        </p>
                      </div>
                    )}
                    <input
                      required
                      accept="image/x-png,image/jpg,image/jpeg"
                      type="file"
                      className="opacity-0"
                      onChange={handleChange}
                    />
                  </label>
                </div>
              </div>
            </div>
          </div>
          <div className="mt-8 flex items-center justify-center">
            <button
              className="hover:text-blue-700 text-blood ring-2 ring-current font-bold py-2 px-4 rounded-xl focus:outline-none focus:shadow-outline"
              type="submit"
            >
              Publier
            </button>
          </div>
        </form>
      </div>
    </div>
  );
}

export default Editor;
