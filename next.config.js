require("dotenv").config();
module.exports = {
  reactStrictMode: true,
  env: {
    SERVER: process.env.SERVER,
  },
  async rewrites() {
    return [
      {
        source: "/api/:path*",
        destination: process.env.SERVER + "/api/:path*",
      },
    ];
  },
};

const removeImports = require("next-remove-imports")();

module.exports = removeImports({
  experimental: { esmExternals: true },
});
