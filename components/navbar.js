/* eslint-disable @next/next/no-img-element */
import Link from "next/link";
import { Fragment, useContext } from "react";
import { Popover, Transition } from "@headlessui/react";
import {
  BookmarkAltIcon,
  CalendarIcon,
  ChartBarIcon,
  CursorClickIcon,
  MenuIcon,
  RefreshIcon,
  ShieldCheckIcon,
  SupportIcon,
  ViewGridIcon,
  XIcon,
} from "@heroicons/react/outline";
import { ChevronDownIcon } from "@heroicons/react/solid";
import AppContext from "../context/AppContext";
import Cookies from "js-cookie";

const solutions = [
  {
    name: "SEO",
    description: "Optimisez votre front-end pour le référencement naturel",
    href: "#",
    icon: ChartBarIcon,
  },
  {
    name: "Interactions",
    description: "Ajoutez des interactions utilisateur",
    href: "#",
    icon: CursorClickIcon,
  },
  {
    name: "Authentification",
    description: "Mettez en place un système d'authentification sécurisée",
    href: "#",
    icon: ShieldCheckIcon,
  },
  {
    name: "Intégrations",
    description: "Intégrez des fonctionnalités externes à votre site",
    href: "#",
    icon: ViewGridIcon,
  },
  {
    name: "Automatisation",
    description:
      "Développez des systèmes automatisés pour fluidifier votre workflow",
    href: "#",
    icon: RefreshIcon,
  },
];
const resources = [
  {
    name: "API",
    description: "Développez le coeur de votre back-end",
    href: "#",
    icon: SupportIcon,
  },
  {
    name: "Guides",
    description: "Suivez les guides pour établir une API selon vos besoins",
    href: "#",
    icon: BookmarkAltIcon,
  },
  {
    name: "Bases de données",
    description: "Installez une BDD pour enregistrer les données de votre site",
    href: "#",
    icon: CalendarIcon,
  },
  {
    name: "Sécurité",
    description:
      "Mettez en place les recommandations de sécurité dans votre serveur",
    href: "#",
    icon: ShieldCheckIcon,
  },
];

function classNames(...classes) {
  return classes.filter(Boolean).join(" ");
}

export default function Navbar() {
  const context = useContext(AppContext);
  function handleLogout() {
    Cookies.remove("jwt");
    Cookies.remove("user");
    window.location.href = "/";
  }

  return (
    <Popover className="relative bg-black z-50">
      <div className="max-w-7xl mx-auto px-4 sm:px-6">
        <div className="flex justify-between items-center py-6 md:justify-start md:space-x-10">
          <div className="flex justify-start lg:w-0 lg:flex-1">
            <Link href="/">
              <a>
                <span className="sr-only">Full Steak</span>
                <img className="w-10 sm:h-10" src="/steak.svg" alt="" />
              </a>
            </Link>
          </div>
          <div className="-mr-2 -my-2 md:hidden">
            <Popover.Button className=" rounded-md p-2 inline-flex items-center justify-center text-white hover:text-white hover:bg-gray-100 focus:outline-none focus:ring-2 focus:ring-inset focus:ring-indigo-500">
              <span className="sr-only">Ouvrir le menu</span>
              <MenuIcon className="h-6 w-6" aria-hidden="true" />
            </Popover.Button>
          </div>
          <Popover.Group as="nav" className="hidden md:flex space-x-10">
            <Link href="/">
              <a className="text-base font-medium text-white hover:text-gold">
                FullSteak
              </a>
            </Link>
            <Link href="#">
              <a className="text-base font-medium text-white hover:text-gold">
                Docs
              </a>
            </Link>
            <Popover className="relative">
              {({ open }) => (
                <>
                  <Popover.Button
                    className={classNames(
                      "text-white group rounded-md inline-flex items-center text-base font-medium"
                    )}
                  >
                    <span>Front</span>
                    <ChevronDownIcon
                      className={classNames(
                        open ? "text-gold" : "text-gray-400",
                        "ml-2 h-5 w-5 group-hover:text-gold"
                      )}
                      aria-hidden="true"
                    />
                  </Popover.Button>

                  <Transition
                    as={Fragment}
                    enter="transition ease-out duration-200"
                    enterFrom="opacity-0 translate-y-1"
                    enterTo="opacity-100 translate-y-0"
                    leave="transition ease-in duration-150"
                    leaveFrom="opacity-100 translate-y-0"
                    leaveTo="opacity-0 translate-y-1"
                  >
                    <Popover.Panel className="absolute z-10 -ml-4 mt-3 transform px-2 w-screen max-w-md sm:px-0 lg:ml-0 lg:left-1/2 lg:-translate-x-1/2">
                      <div className="rounded-lg shadow-lg ring-1 ring-black ring-opacity-5 overflow-hidden">
                        <div className="relative grid gap-6 bg-white px-5 py-6 sm:gap-8 sm:p-8 text-black">
                          {solutions.map((item) => (
                            <Link key={item.name} href={item.href}>
                              <a className="-m-3 p-3 flex items-start rounded-lg hover:bg-gray-50">
                                <item.icon
                                  className="flex-shrink-0 h-6 w-6 text-indigo-600"
                                  aria-hidden="true"
                                />
                                <div className="ml-4">
                                  <p className="text-base font-medium text-gray-900">
                                    {item.name}
                                  </p>
                                  <p className="mt-1 text-sm text-black">
                                    {item.description}
                                  </p>
                                </div>
                              </a>
                            </Link>
                          ))}
                        </div>
                      </div>
                    </Popover.Panel>
                  </Transition>
                </>
              )}
            </Popover>

            <Popover className="relative">
              {({ open }) => (
                <>
                  <Popover.Button
                    className={classNames(
                      "group text-white rounded-md inline-flex items-center text-base font-medium"
                    )}
                  >
                    <span>Back</span>
                    <ChevronDownIcon
                      className={classNames(
                        open ? "text-gold" : "text-gray-400",
                        "ml-2 h-5 w-5 group-hover:text-gold"
                      )}
                      aria-hidden="true"
                    />
                  </Popover.Button>

                  <Transition
                    as={Fragment}
                    enter="transition ease-out duration-200"
                    enterFrom="opacity-0 translate-y-1"
                    enterTo="opacity-100 translate-y-0"
                    leave="transition ease-in duration-150"
                    leaveFrom="opacity-100 translate-y-0"
                    leaveTo="opacity-0 translate-y-1"
                  >
                    <Popover.Panel className="absolute z-10 left-1/2 transform -translate-x-1/2 mt-3 px-2 w-screen max-w-md sm:px-0">
                      <div className="rounded-lg shadow-lg ring-1 ring-black ring-opacity-5 overflow-hidden">
                        <div className="relative grid gap-6 bg-white px-5 py-6 sm:gap-8 sm:p-8">
                          {resources.map((item) => (
                            <Link href={item.href} key={item.name}>
                              <a className="-m-3 p-3 flex items-start rounded-lg hover:bg-gray-50">
                                <item.icon
                                  className="flex-shrink-0 h-6 w-6 text-indigo-600"
                                  aria-hidden="true"
                                />
                                <div className="ml-4">
                                  <p className="text-base font-medium text-gray-900">
                                    {item.name}
                                  </p>
                                  <p className="mt-1 text-sm text-black">
                                    {item.description}
                                  </p>
                                </div>
                              </a>
                            </Link>
                          ))}
                        </div>
                      </div>
                    </Popover.Panel>
                  </Transition>
                </>
              )}
            </Popover>
          </Popover.Group>
          <div className="hidden md:flex items-center justify-end md:flex-1 lg:w-0">
            {context.isAuthenticated ? (
              <button
                onClick={handleLogout}
                className="whitespace-nowrap text-base font-medium text-white hover:text-gold"
              >
                Se déconnecter
              </button>
            ) : (
              <Link href="/register">
                <a className="whitespace-nowrap text-base font-medium text-white hover:text-gold">
                  S&apos;inscrire
                </a>
              </Link>
            )}

            {context.isAuthenticated ? (
              <Link href="/editor">
                <a className="ml-8 whitespace-nowrap inline-flex items-center justify-center px-4 py-2 border border-transparent rounded-md shadow-sm text-base font-medium text-gold border-gold hover:bg-blood hover:text-gold">
                  {context.user.username}
                </a>
              </Link>
            ) : (
              <Link href="/login">
                <a className="ml-8 whitespace-nowrap inline-flex items-center justify-center px-4 py-2 border border-transparent rounded-md shadow-sm text-base font-medium text-gold border-gold hover:bg-blood">
                  Connexion
                </a>
              </Link>
            )}
          </div>
        </div>
      </div>

      <Transition
        as={Fragment}
        enter="duration-200 ease-out"
        enterFrom="opacity-0 scale-95"
        enterTo="opacity-100 scale-100"
        leave="duration-100 ease-in"
        leaveFrom="opacity-100 scale-100"
        leaveTo="opacity-0 scale-95"
      >
        <Popover.Panel
          focus
          className="absolute top-0 inset-x-0 p-2 transition transform origin-top-right md:hidden"
        >
          <div className="rounded-lg shadow-lg ring-1 ring-black ring-opacity-5 bg-white divide-y-2 divide-gray-50">
            <div className="pt-5 pb-6 px-5">
              <div className="flex items-center justify-between">
                <div>
                  <img className="h-8 w-auto" src="/steak.svg" alt="Workflow" />
                </div>
                <div className="-mr-2">
                  <Popover.Button className="bg-white rounded-md p-2 inline-flex items-center justify-center text-gray-400 hover:text-white hover:bg-gray-100 focus:outline-none focus:ring-2 focus:ring-inset focus:ring-indigo-500">
                    <span className="sr-only">Close menu</span>
                    <XIcon className="h-6 w-6" aria-hidden="true" />
                  </Popover.Button>
                </div>
              </div>
              <div className="mt-6">
                <nav className="grid gap-y-8">
                  {solutions.map((item) => (
                    <Link key={item.name} href={item.href}>
                      <a className="-m-3 p-3 flex items-center rounded-md hover:bg-gray-50">
                        <item.icon
                          className="flex-shrink-0 h-6 w-6 text-indigo-600"
                          aria-hidden="true"
                        />
                        <span className="ml-3 text-base font-medium text-gray-900">
                          {item.name}
                        </span>
                      </a>
                    </Link>
                  ))}
                </nav>
              </div>
            </div>
            <div className="py-6 px-5 space-y-6">
              <div className="grid grid-cols-2 gap-y-4 gap-x-8">
                <Link href="/">
                  <a className="text-base font-medium text-gray-900 hover:text-gray-700">
                    Full Steak
                  </a>
                </Link>

                <Link href="#">
                  <a className="text-base font-medium text-gray-900 hover:text-gray-700">
                    Docs
                  </a>
                </Link>
                {resources.map((item) => (
                  <Link key={item.name} href={item.href}>
                    <a className="text-base font-medium text-gray-900 hover:text-gray-700">
                      {item.name}
                    </a>
                  </Link>
                ))}
              </div>
              <div>
                {context.isAuthenticated ? (
                  <button
                    onClick={handleLogout}
                    className="w-full flex items-center justify-center px-4 py-2 border border-transparent rounded-md shadow-sm text-base font-medium text-blood border-blood"
                  >
                    Se déconnecter
                  </button>
                ) : (
                  <Link href="/register">
                    <a className="w-full flex items-center justify-center px-4 py-2 border border-transparent rounded-md shadow-sm text-base font-medium text-blood border-blood">
                      S&apos;inscrire
                    </a>
                  </Link>
                )}

                <p className="mt-6 text-center text-base font-medium text-white">
                  {context.isAuthenticated ? (
                    <Link href="/editor">
                      <a className="text-blood hover:text-indigo-500">
                        {context.user.username}
                      </a>
                    </Link>
                  ) : (
                    <Link href="/login">
                      <a className="text-blood hover:text-indigo-500">
                        Connexion
                      </a>
                    </Link>
                  )}
                </p>
              </div>
            </div>
          </div>
        </Popover.Panel>
      </Transition>
    </Popover>
  );
}
