/* eslint-disable jsx-a11y/alt-text */
/* eslint-disable @next/next/no-img-element */
import Link from "next/link";
import { dateToString } from "../utils/dateParser";

export default function Latest(props) {
  function buildArticle(article) {
    return (
      <div
        key={article.title}
        className="flex flex-col items-start col-span-12 space-y-3 sm:col-span-6 xl:col-span-4"
      >
        <Link href={"/article/" + article.id}>
          <a className="block">
            <img
              alt={article.image.name}
              className="object-cover w-full mb-2 overflow-hidden rounded-lg shadow-sm max-h-56"
              src={`https://api.fullsteak.fr${article.image.formats.medium.url}`}
            />
          </a>
        </Link>
        <div className="flex flex-row justify-items-start space-x-2">
          {article.steak.map((technology) => {
            return (
              <div
                key={technology}
                className="text-blood bg-gold hover:bg-blood hover:text-gold items-center px-3 py-1.5 leading-none rounded-full text-xs font-medium uppercase inline-block"
              >
                <span>{technology}</span>
              </div>
            );
          })}
        </div>

        <h2 className="text-lg font-bold sm:text-xl md:text-2xl">
          <Link href={"/article/" + article.id}>
            <a>{article.title}</a>
          </Link>
        </h2>
        <p className="text-sm text-gray-500">{article.description}</p>
        <p className="pt-2 text-xs font-medium">
          <Link href={"/article/" + article.id}>
            <a className="mr-1 underline">{article.author.username}</a>
          </Link>{" "}
          ·{" "}
          <span className="mx-1">
            {dateToString(new Date(article.created_at))}
          </span>{" "}
          · <span className="mx-1 text-gray-600">5 min</span>
        </p>
      </div>
    );
  }

  function buildTopArticle(article) {
    return (
      <div className="flex flex-col items-center sm:px-5 md:flex-row">
        <div className="w-full md:w-1/2">
          <Link href={"/article/" + article.id}>
            <a className="block">
              <img
                alt={article.image.name}
                className="object-cover w-full h-full rounded-lg max-h-64 sm:max-h-96"
                src={`https://api.fullsteak.fr${article.image.formats.medium.url}`}
              />
            </a>
          </Link>
        </div>
        <div className="flex flex-col items-start justify-center w-full h-full py-6 mb-6 md:mb-0 md:w-1/2">
          <div className="flex flex-col items-start justify-center h-full space-y-3 transform md:pl-10 lg:pl-16 md:space-y-5">
            <div className="flex flex-row justify-items-start space-x-2">
              {article.steak.map((technology) => {
                return (
                  <div
                    key={technology}
                    className="bg-black flex items-center pl-2 pr-3 py-1.5 leading-none rounded-full text-xs font-medium uppercase text-white"
                  >
                    <svg
                      className="w-3.5 h-3.5 mr-1"
                      fill="currentColor"
                      viewBox="0 0 20 20"
                      xmlns="http://www.w3.org/2000/svg"
                    >
                      <path d="M9.049 2.927c.3-.921 1.603-.921 1.902 0l1.07 3.292a1 1 0 00.95.69h3.462c.969 0 1.371 1.24.588 1.81l-2.8 2.034a1 1 0 00-.364 1.118l1.07 3.292c.3.921-.755 1.688-1.54 1.118l-2.8-2.034a1 1 0 00-1.175 0l-2.8 2.034c-.784.57-1.838-.197-1.539-1.118l1.07-3.292a1 1 0 00-.364-1.118L2.98 8.72c-.783-.57-.38-1.81.588-1.81h3.461a1 1 0 00.951-.69l1.07-3.292z"></path>
                    </svg>
                    <span>{technology}</span>
                  </div>
                );
              })}
            </div>

            <h1 className="text-4xl font-bold leading-none lg:text-5xl xl:text-6xl">
              <Link href={"/article/" + article.id}>
                <a>{article.title}</a>
              </Link>
            </h1>
            <p className="pt-2 text-sm font-medium">
              par{" "}
              <Link href={"/article/" + article.id}>
                <a className="mr-1 underline">{article.author.username}</a>
              </Link>{" "}
              ·{" "}
              <span className="mx-1">
                {dateToString(new Date(article.created_at))}
              </span>{" "}
              · <span className="mx-1 text-gray-600">5 min.</span>
            </p>
          </div>
        </div>
      </div>
    );
  }

  return (
    <section className="">
      <div className="w-full px-5 py-6 mx-auto space-y-5 sm:py-8 md:py-12 sm:space-y-8 md:space-y-16 max-w-7xl">
        {buildTopArticle(props.articles[0])}
        <div className="grid grid-cols-12 pb-10 sm:px-5 gap-x-8 gap-y-16">
          {props.articles.map((article, i) => {
            if (i > 0) return buildArticle(article);
          })}
        </div>
      </div>
    </section>
  );
}
